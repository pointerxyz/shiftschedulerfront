import axios from 'axios'

export default {
  getIncidents(params) {
    return axios.get('/incidents', {
      params
    })
  }
}
