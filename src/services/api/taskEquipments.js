import axios from 'axios'

export default {
  getAllTaskEquipments() {
    return axios.get('/taskequipments/all')
  }
}
