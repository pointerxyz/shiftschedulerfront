import axios from 'axios'

export default {
  getAllSchedule() {
    return axios.get('/scheduler/all')
  }
}
