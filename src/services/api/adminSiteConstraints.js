import axios from 'axios'

export default {
  getSiteConstraints(params) {
    return axios.get('/siteconstraint', {
      params
    })
  },
  editSiteConstraint(id, payload) {
    return axios.patch(`/siteconstraint/${id}`, payload)
  },
  saveSiteConstraint(payload) {
    return axios.post('/siteconstraint/', payload)
  },
  deleteSiteConstraint(id) {
    return axios.delete(`/siteconstraint/${id}`)
  }
}
