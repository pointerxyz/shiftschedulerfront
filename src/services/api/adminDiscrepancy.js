import axios from 'axios'

export default {
  getDiscrepancies(params) {
    return axios.get('/discrepancy', {
      params
    })
  }
}
