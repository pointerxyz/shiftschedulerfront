import axios from 'axios'

export default {
  generateSchedules(params) {
    // console.log(payload)
    return axios.get('/scheduler', {
      params
    })
  },
  editSchedule(id, payload) {
    return axios.patch(`/scheduler/${id}`, payload)
  },
  saveSchedule(payload) {
    return axios.post('/scheduler/', payload)
  },
  deleteSchedule(id) {
    return axios.delete(`/scheduler/${id}`)
  }
}
