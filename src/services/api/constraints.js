import axios from 'axios'

export default {
  getAllAgentConstraints() {
    return axios.get('/agentconstraint/all')
  }
}
