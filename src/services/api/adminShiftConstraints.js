import axios from 'axios'

export default {
  getShiftConstraints(params) {
    // console.log(params)
    return axios.get('/shiftconstraint', {
      params
    })
  },
  editShiftConstraint(id, payload) {
    return axios.patch(`/shiftconstraint/${id}`, payload)
  },
  saveShiftConstraint(payload) {
    return axios.post('/shiftconstraint/', payload)
  },
  deleteShiftConstraint(id) {
    return axios.delete(`/shiftconstraint/${id}`)
  }
}
