import axios from 'axios'

export default {
  getShiftTypes(params) {
    return axios.get('/shifttype', {
      params
    })
  },
  editShiftType(id, payload) {
    return axios.patch(`/shifttype/${id}`, payload)
  },
  saveShiftType(payload) {
    // console.log(payload)
    return axios.post('/shifttype/', payload)
  },
  deleteShiftType(id) {
    return axios.delete(`/shifttype/${id}`)
  }
}
