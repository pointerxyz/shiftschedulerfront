import axios from 'axios'

export default {
  getSiteSchedules(params) {
    return axios.get('/siteschedules', {
      params
    })
  },
  editSiteschedule(id, payload) {
    return axios.patch(`/siteschedules/${id}`, payload)
  },
  saveSiteschedule(payload) {
    console.log(payload)
    return axios.post('/siteschedules/', payload)
  },
  deleteSiteschedule(id) {
    return axios.delete(`/siteschedules/${id}`)
  }
}
