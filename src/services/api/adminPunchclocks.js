import axios from 'axios'

export default {
  getPunchclocks(params) {
    return axios.get('validatepunches', {
      params
    })
  },
  validatePunches(params) {
    return axios.post('validatepunches', {
      params
    })
  }
}
