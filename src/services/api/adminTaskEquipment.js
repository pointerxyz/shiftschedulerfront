import axios from 'axios'

export default {
  getTaskEquipments(params) {
    return axios.get('/taskequipments', {
      params
    })
  },
  editTaskEquipment(id, payload) {
    return axios.patch(`/taskequipments/${id}`, payload)
  },
  saveTaskEquipment(payload) {
    // console.log(payload)
    return axios.post('/taskequipments/', payload)
  },
  deleteTaskEquipment(id) {
    return axios.delete(`/taskequipments/${id}`)
  }
}
