import axios from 'axios'

export default {
  getCoefficients(params) {
    return axios.get('/coefficients', {
      params
    })
  }
}
