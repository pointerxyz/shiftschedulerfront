import axios from 'axios'

export default {
  getCumulHours(params) {
    return axios.get('cumulhours', { params })
  }
}
