import axios from 'axios'

export default {
  getExcessDemand(params) {
    return axios.get('/excessdemand', {
      params
    })
  }
}
