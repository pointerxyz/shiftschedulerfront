import axios from 'axios'

export default {
  getPenalties(params) {
    return axios.get('/penalties', {
      params
    })
  }
}
