import axios from 'axios'

export default {
  getScheduleagents(params) {
    return axios.get('/scheduleagents', {
      params
    })
  },
  editScheduleagent(id, payload) {
    return axios.patch(`/scheduleagents/${id}`, payload)
  },
  saveScheduleagent(payload) {
    console.log(payload)
    return axios.post('/scheduleagents/', payload)
  },
  deleteScheduleagent(id) {
    return axios.delete(`/scheduleagents/${id}`)
  }
}
