import axios from 'axios'

export default {
  getDaybooks(params) {
    return axios.get('/daybook', {
      params
    })
  }
}
