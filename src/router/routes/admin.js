// import VueRouter from 'vue-router'
import Bar from '../../components/Bar.vue'
import Line from '../../components/Line.vue'
import Pie from '../../components/Pie.vue'
import Bubble from './../../components/Bubble.vue'

export default [
  // const routes = [
  {
    path: '/admin/business',
    name: 'admin-business',
    component: () =>
      import(
        /* webpackChunkName: "admin-business" */ '@/components/AdminBusinesses.vue'
      ),
    meta: {
      title: 'Business',
      requiresAuth: true,
      keepAlive: false
    }
  },
  {
    path: '/admin/cities',
    name: 'admin-cities',
    meta: {
      title: 'Villes',
      keepAlive: false,
      requiresAuth: true
    },
    component: () =>
      import(
        /* webpackChunkName: "admin-cities" */
        '@/components/AdminCities.vue'
      )
  },
  {
    path: '/admin/users',
    name: 'admin-users',
    meta: {
      title: 'Agents',
      requiresAuth: true
    },
    component: () =>
      import(
        /* webpackChunkName: "admin-users" */ '@/components/AdminUsers.vue'
      )
  },
  {
    path: '/admin/tasks',
    name: 'admin-tasks',
    meta: {
      title: 'Taches',
      requiresAuth: true
    },
    component: () =>
      import(
        /* webpackChunkName: "admin-tasks" */
        '@/components/AdminTasks.vue'
      )
  },
  {
    path: '/admin/equipment',
    name: 'admin-equipment',
    meta: {
      title: 'Equipements',
      requiresAuth: true
    },
    component: () =>
      import(
        /* webpackChunkName: "admin-tasks" */
        '@/components/AdminEquipment.vue'
      )
  },
  {
    path: '/admin/task-equipment',
    name: 'admin-task-equipment',
    meta: {
      title: 'Equipement/Tache',
      requiresAuth: true
    },
    component: () =>
      import(
        /* webpackChunkName: "admin-tasks" */
        '@/components/AdminTaskEquipment.vue'
      )
  },
  {
    path: '/admin/sites',
    name: 'admin-sites',
    meta: {
      title: 'Sites',
      requiresAuth: true
    },
    component: () =>
      import(
        /* webpackChunkName: "admin-sites" */ '@/components/AdminSites.vue'
      )
  },
  {
    path: '/admin/assignments',
    name: 'admin-assignments',
    meta: {
      title: 'Vacations',
      requiresAuth: true
    },
    component: () =>
      import(
        /* webpackChunkName: "admin-assignments" */
        '@/components/AdminAssignments.vue'
      )
  },
  {
    path: '/admin/shiftconstraints',
    name: 'admin-shift-constraints',
    meta: {
      title: 'Containtes shift',
      requiresAuth: true
    },
    component: () =>
      import(
        /* webpackChunkName: "admin-assignments" */
        '@/components/AdminShiftConstraints.vue'
      )
  },
  {
    path: '/admin/siteconstraints',
    name: 'admin-site-constraints',
    meta: {
      title: 'Containtes Site',
      requiresAuth: true
    },
    component: () =>
      import(
        /* webpackChunkName: "admin-assignments" */
        '@/components/AdminSiteConstraints.vue'
      )
  },
  {
    path: '/admin/track-availability',
    name: 'admin-track-availability',
    meta: {
      title: 'Disponibilités',
      requiresAuth: true
    },
    component: () =>
      import(
        /* webpackChunkName: "admin-assignments" */
        '@/components/Notyet.vue'
      )
  },
  {
    path: '/admin/distribute-hours',
    name: 'admin-distribute-hours',
    meta: {
      title: 'Horaires distribution',
      requiresAuth: true
    },
    component: () =>
      import(
        /* webpackChunkName: "admin-assignments" */
        '@/components/Notyet.vue'
      )
  },
  {
    path: '/admin/days-off-control',
    name: 'admin-days-off-control',
    meta: {
      title: 'Controle Jour Sans',
      requiresAuth: true
    },
    component: () =>
      import(
        /* webpackChunkName: "admin-assignments" */
        '@/components/Notyet.vue'
      )
  },
  {
    path: '/admin/trade-shifts',
    name: 'admin-trade-shifts',
    meta: {
      title: 'Echanges Shift',
      requiresAuth: true
    },
    component: () =>
      import(
        /* webpackChunkName: "admin-assignments" */
        '@/components/Notyet.vue'
      )
  },
  {
    path: '/admin/smart-notes',
    name: 'admin-smart-notes',
    meta: {
      title: 'Notes',
      requiresAuth: true
    },
    component: () =>
      import(
        /* webpackChunkName: "admin-assignments" */
        '@/components/Notyet.vue'
      )
  },
  {
    path: '/admin/discrepancies',
    name: 'admin-discrepancies',
    meta: {
      title: 'Ecarts',
      requiresAuth: true
    },
    component: () =>
      import(
        /* webpackChunkName: "admin-assignments" */
        '@/components/Notyet.vue'
      )
  },
  {
    path: '/admin/agentschedules',
    name: 'admin-agent-schedules',
    meta: {
      title: 'Planification Agents',
      requiresAuth: true
    },
    component: () =>
      import(
        /* webpackChunkName: "admin-assignments" */
        '@/components/AdminAgentSchedules.vue'
      )
  },
  {
    path: '/admin/siteschedules',
    name: 'admin-site-schedules',
    meta: {
      title: 'Planification Site',
      requiresAuth: true
    },
    component: () =>
      import(
        /* webpackChunkName: "admin-assignments" */
        '@/components/AdminSiteSchedules.vue'
      )
  },
  {
    path: '/admin/coefficients',
    name: 'admin-coefficients',
    meta: {
      title: 'Coefficients',
      requiresAuth: true
    },
    component: () =>
      import(
        /* webpackChunkName: "admin-assignments" */
        '@/components/Notyet.vue'
      )
  },
  {
    path: '/admin/excessdemand',
    name: 'admin-excess-demand',
    meta: {
      title: 'Exces Demandes',
      requiresAuth: true
    },
    component: () =>
      import(
        /* webpackChunkName: "admin-assignments" */
        '@/components/Notyet.vue'
      )
  },
  {
    path: '/admin/incident',
    name: 'admin-incidents',
    meta: {
      title: 'Incidents',
      requiresAuth: true
    },
    component: () =>
      import(
        /* webpackChunkName: "admin-assignments" */
        '@/components/AdminIncident.vue'
      )
  },
  {
    path: '/admin/daybook',
    name: 'admin-daybooks',
    meta: {
      title: 'Daybooks',
      requiresAuth: true
    },
    component: () =>
      import(
        /* webpackChunkName: "admin-assignments" */
        '@/components/AdminDaybooks.vue'
      )
  },
  {
    path: '/admin/metric-daybooks',
    name: 'metric-daybooks',
    meta: {
      title: 'Stats Daybooks',
      requiresAuth: true
    },
    component: () =>
      import(
        /* webpackChunkName: "admin-assignments" */
        '@/components/BarChart.vue'
      )
  },
  {
    path: '/admin/metric-incidents',
    name: 'metric-incidents',
    meta: {
      title: 'Stats Incidents',
      requiresAuth: true
    },
    component: () =>
      import(
        /* webpackChunkName: "admin-assignments" */
        '@/components/AreaChart.vue'
      )
  },
  {
    path: '/admin/metric-clientts',
    name: 'metric-clients',
    meta: {
      title: 'Stats Clients',
      requiresAuth: true
    },
    component: () =>
      import(
        /* webpackChunkName: "admin-assignments" */
        '@/components/PieChart.vue'
      )
  },
  {
    path: '/admin/metric-trade-shifts',
    name: 'metric-trade-shifts',
    meta: {
      title: 'Stats Trade Shifts',
      requiresAuth: true
    },
    component: () =>
      import(
        /* webpackChunkName: "admin-assignments" */
        '@/components/RadarChart.vue'
      )
  },
  {
    path: '/admin/metric-dash',
    name: 'metric-dash',
    meta: {
      title: 'Dashboard',
      requiresAuth: true
    },
    component: () =>
      import(
        /* webpackChunkName: "admin-assignments" */
        '@/components/AdminCharts.vue'
      )
  },
  {
    path: '/admin/control-track-availability',
    name: 'control-track-availability',
    meta: {
      title: 'Controle Disponibilités',
      requiresAuth: true
    },
    component: () =>
      import(
        /* webpackChunkName: "admin-assignments" */
        '@/components/Notyet.vue'
      )
  },
  {
    path: '/admin/control-distribute-hours',
    name: 'control-distribute-hours',
    meta: {
      title: 'Controle des Heures',
      requiresAuth: true
    },
    component: () =>
      import(
        /* webpackChunkName: "admin-assignments" */
        '@/components/Notyet.vue'
      )
  },
  {
    path: '/admin/control-days-off-control',
    name: 'control-days-off-control',
    meta: {
      title: 'Controle Jours Off',
      requiresAuth: true
    },
    component: () =>
      import(
        /* webpackChunkName: "admin-assignments" */
        '@/components/Notyet.vue'
      )
  },
  {
    path: '/admin/control-trade-shifts',
    name: 'control-trade-shifts',
    meta: {
      title: 'Controle Echange Shifts',
      requiresAuth: true
    },
    component: () =>
      import(
        /* webpackChunkName: "admin-assignments" */
        '@/components/Notyet.vue'
      )
  },
  {
    path: '/admin/control-smart-notes',
    name: 'control-smart-notes',
    meta: {
      title: 'Smart Notes',
      requiresAuth: true
    },
    component: () =>
      import(
        /* webpackChunkName: "admin-assignments" */
        '@/components/Notyet.vue'
      )
  },
  {
    path: '/admin/control-excess-demand',
    name: 'control-excess-demand',
    meta: {
      title: 'Demandes en Excés',
      requiresAuth: true
    },
    component: () =>
      import(
        /* webpackChunkName: "admin-assignments" */
        '@/components/Notyet.vue'
      )
  },
  {
    path: '/admin/control-coefficients',
    name: 'control-coefficients',
    meta: {
      title: 'Controle Coefficients',
      requiresAuth: true
    },
    component: () =>
      import(
        /* webpackChunkName: "admin-assignments" */
        '@/components/Notyet.vue'
      )
  },
  {
    path: '/manage/penalties',
    name: 'manage-penalties',
    meta: {
      title: 'Controle Divergences',
      requiresAuth: true
    },
    component: () =>
      import(
        /* webpackChunkName: "admin-assignments" */
        '@/components/AdminPenalties.vue'
      )
  },
  {
    path: '/admin/dashboard',
    name: 'admin-dashboard',
    meta: {
      title: 'Dashboard',
      requiresAuth: true
    },
    component: () => import('@/components/AdminDashboard')
    // children: [
    //   {
    //     name: 'Dashboard',
    //     path: '',
    //     component: () => import('@/views/dashboard/Dashboard')
    //   },
    //   {
    //     name: 'User Profile',
    //     path: 'pages/user',
    //     component: () => import('@/views/dashboard/pages/UserProfile')
    //   },
    //   {
    //     name: 'Notifications',
    //     path: 'components/notifications',
    //     component: () => import('@/views/dashboard/component/Notifications')
    //   },
    //   {
    //     name: 'Google Maps',
    //     path: 'maps/google-maps',
    //     component: () => import('@/views/dashboard/maps/GoogleMaps')
    //   }
    // ]
  },
  {
    path: '/admin/metric-dash-line',
    name: 'metric-dash-line',
    meta: {
      title: 'Dashboard Line',
      requiresAuth: true
    },
    component: () =>
      import(
        /* webpackChunkName: "admin-assignments" */
        '@/components/Line.vue'
      )
  },
  {
    path: '/admin/metric-dash-pie',
    name: 'metric-dash-pie',
    meta: {
      title: 'Dashboard Pie',
      requiresAuth: true
    },
    component: () =>
      import(
        /* webpackChunkName: "admin-assignments" */
        '@/components/Pie.vue'
      )
  },
  {
    path: '/admin/metric-dash-bubble',
    name: 'metric-dash-bubble',
    meta: {
      title: 'Dashboard Bubble',
      requiresAuth: true
    },
    component: () =>
      import(
        /* webpackChunkName: "admin-assignments" */
        '@/components/Bubble.vue'
      )
  },
  {
    path: '/admin/punchclocks',
    name: 'admin-punchclocks',
    meta: {
      title: 'Punchclocks',
      requiresAuth: true
    },
    component: () =>
      import(
        /* webpackChunkName: "admin-assignments" */
        '@/components/AdminPunchclocks.vue'
      )
  },
  {
    path: '/admin/boundaries',
    name: 'admin-boundaries',
    meta: {
      title: 'Boundaries',
      requiresAuth: true
    },
    component: () =>
      import(
        /* webpackChunkName: "admin-assignments" */
        '@/views/Boundaries.vue'
      )
  },
  {
    path: '/admin/representatives',
    name: 'admin-representatives',
    meta: {
      title: 'Representatives',
      requiresAuth: true
    },
    component: () =>
      import(
        /* webpackChunkName: "admin-assignments" */
        '@/views/Representatives.vue'
      )
  },
  {
    path: '/admin/client-requests',
    name: 'admin-client-requests',
    meta: {
      title: 'Client requests',
      requiresAuth: true
    },
    component: () =>
      import(
        /* webpackChunkName: "admin-assignments" */
        '@/components/AdminRequests.vue'
      )
  },
  {
    path: '/admin/system-settings',
    name: 'admin-system-settings',
    meta: {
      title: 'System Settings',
      requiresAuth: true
    },
    component: () =>
      import(
        /* webpackChunkName: "admin-assignments" */
        '@/components/Notyet.vue'
      )
  },
  {
    path: '/admin/cumul-hours',
    name: 'admin-cumul-hours',
    meta: {
      title: 'Cumul hours',
      requiresAuth: true
    },
    component: () =>
      import(
        /* webpackChunkName: "admin-assignments" */
        '@/components/AdminCumulhours.vue'
      )
  }
]

// export default new VueRouter({
//   routes
// })
