import Vue from 'vue'
import '@/plugins/axios'
// import '@/plugins/vuetify'
// import 'material-design-icons-iconfont/dist/material-design-icons.css'
// import '@mdi/font/css/materialdesignicons.css'
import VueGeolocation from 'vue-browser-geolocation'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import './plugins/base'
import './plugins/chartist'
import 'vuetify/dist/vuetify.min.css'
import './plugins/vuetify'
import '@/plugins/veevalidate'
import '@/plugins/common'
// import '@/plugins/googleAnalytics'
import VuetifyDialog from 'vuetify-dialog'
import 'vuetify-dialog/dist/vuetify-dialog.min.css'
import i18n from '@/plugins/i18n'
import App from '@/App.vue'
import router from '@/router'
import { store } from '@/store'
import Vuetify from 'vuetify'
import VueTextareaAutosize from 'vue-textarea-autosize'

Vue.use(VueTextareaAutosize)
Vue.use(BootstrapVue)
Vue.use(VueGeolocation)
Vue.config.productionTip = false

Vue.use(Vuetify)
Vue.use(VuetifyDialog, {
  context: {
    Vuetify
  }
})
const opts = {
  theme: {
    dark: true,
    themes: {
      light: {},
      dark: {}
    }
  }
}
console.log(process.env.NODE_ENV) // dev
const ignoreWarnMessage =
  'The .native modifier for v-on is only valid on components but it was used on <div>.'
Vue.config.warnHandler = function (msg, vm, trace) {
  // `trace` is the component hierarchy trace
  if (msg === ignoreWarnMessage) {
    msg = null
    vm = null
    trace = null
  }
}

export const app = new Vue({
  router,
  store,
  i18n,
  vuetify: new Vuetify(opts),
  render: (h) => h(App),
  created() {
    store.dispatch('setLocale', store.getters.locale)
    if (store.getters.isTokenSet) {
      store.dispatch('autoLogin')
    }
  }
}).$mount('#app')

if (window.Cypress) {
  // Only available during E2E tests
  window.app = app
}
