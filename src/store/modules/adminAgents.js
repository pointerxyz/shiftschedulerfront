import * as types from '@/store/mutation-types'
import api from '@/services/api/adminAgents'
import { buildSuccess, handleError } from '@/utils/utils.js'

const getters = {
  agents: (state) => state.agents,
  totalAgents: (state) => state.totalAgents
}

const actions = {
  getAgents({ commit }, payload) {
    return new Promise((resolve, reject) => {
      api
        .getAgents(payload)
        .then((response) => {
          if (response.status === 200) {
            const userArray = []
            response.data.docs.forEach((element) => {
              userArray.push(element.username)
            })
            // console.log('agents :  ', userArray)
            commit(types.AGENTS, userArray)
            commit(types.TOTAL_AGENTS, response.data.totalDocs)
            resolve()
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  },
  editUser({ commit }, payload) {
    return new Promise((resolve, reject) => {
      const data = {
        username: payload.username,
        email: payload.email,
        role: payload.role,
        phone: payload.phone,
        firstname: payload.firstname,
        lastname: payload.lastname,
        city: payload.city,
        observations: payload.observations,
        skill: payload.skill
      }

      api
        .editUser(payload._id, data)
        .then((response) => {
          if (response.status === 200) {
            buildSuccess(
              {
                msg: 'common.SAVED_SUCCESSFULLY'
              },
              commit,
              resolve
            )
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  },
  saveUser({ commit }, payload) {
    console.log('payload.avatar: ', payload.avatar)
    // let fileType = ''
    // for (const pair of payload.avatar.formData.entries()) {
    //   fileType = pair[1].type
    // }
    // // payload.avatar = {
    // payload.contentType = fileType
    // payload.imageURL = payload.avatar.imageURL
    // }
    // console.log(
    //   'Object.fromEntries : ',
    //   JSON.stringify(Object.fromEntries(payload.formData.entries()))
    // )
    // for (const pair of req.avatar.formData.entries()) {
    //   fileType = pair[1].type
    // }
    // const formData = payload.serializeObject()
    // console.log('serializeObject ', formData)
    // console.log(payload)
    // debugger
    return new Promise((resolve, reject) => {
      api
        .saveUser(payload)
        .then((response) => {
          if (response.status === 201) {
            buildSuccess(
              {
                msg: 'common.SAVED_SUCCESSFULLY'
              },
              commit,
              resolve
            )
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  },
  deleteUser({ commit }, payload) {
    return new Promise((resolve, reject) => {
      api
        .deleteUser(payload)
        .then((response) => {
          if (response.status === 200) {
            buildSuccess(
              {
                msg: 'common.DELETED_SUCCESSFULLY'
              },
              commit,
              resolve
            )
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  }
}

const mutations = {
  [types.AGENTS](state, agents) {
    state.agents = agents
  },
  [types.TOTAL_AGENTS](state, value) {
    state.totalAgents = value
  }
}

const state = {
  agents: [],
  totalAgents: 0
}

export default {
  state,
  getters,
  actions,
  mutations
}
