import * as types from '@/store/mutation-types'
import api from '@/services/api/adminIncidents'
import { buildSuccess, handleError } from '@/utils/utils.js'

const getters = {
  incidents: (state) => state.incidents,
  totalIncidents: (state) => state.totalIncidents
}

const actions = {
  getIncidents({ commit }, payload) {
    return new Promise((resolve, reject) => {
      api
        .getIncidents(payload)
        .then((response) => {
          if (response.status === 200) {
            // console.log(response.data)
            commit(types.INCIDENTS, response.data.docs)
            commit(types.TOTAL_INCIDENTS, response.data.totalDocs)
            resolve()
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  }
}

const mutations = {
  [types.INCIDENTS](state, incidents) {
    state.incidents = incidents
  },
  [types.TOTAL_INCIDENTS](state, value) {
    state.totalIncidents = value
  }
}

const state = {
  incidents: [],
  totalIncidents: 0
}

export default {
  state,
  getters,
  actions,
  mutations
}
