import * as types from '@/store/mutation-types'
import api from '@/services/api/adminSchedule'
import { buildSuccess, handleError } from '@/utils/utils.js'

const getters = {
  schedules: (state) => state.schedules,
  totalSchedule: (state) => state.totalSchedule
}

const actions = {
  generateSchedules({ commit }, payload) {
    return new Promise((resolve, reject) => {
      api
        .generateSchedules(payload)
        .then((response) => {
          if (response.status === 200) {
            buildSuccess(
              {
                msg: 'common.REFRESH'
              },
              commit,
              resolve
            )
            // resolve()
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  },
  editSchedule({ commit }, payload) {
    return new Promise((resolve, reject) => {
      const data = {
        name: payload.name
      }
      api
        .editSchedule(payload._id, data)
        .then((response) => {
          if (response.status === 200) {
            buildSuccess(
              {
                msg: 'common.SAVED_SUCCESSFULLY'
              },
              commit,
              resolve
            )
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  },
  saveSchedule({ commit }, payload) {
    return new Promise((resolve, reject) => {
      api
        .saveSchedule(payload)
        .then((response) => {
          if (response.status === 201) {
            // console.log(response.data)
            commit(types.DISCREPANCIES, response.data.docs)
            commit(types.TOTAL_DISCREPANCIES, response.data.totalDocs)
            buildSuccess(
              {
                msg: 'common.SAVED_SUCCESSFULLY'
              },
              commit,
              resolve
            )
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  },
  deleteSchedule({ commit }, payload) {
    return new Promise((resolve, reject) => {
      api
        .deleteSchedule(payload)
        .then((response) => {
          if (response.status === 200) {
            buildSuccess(
              {
                msg: 'common.DELETED_SUCCESSFULLY'
              },
              commit,
              resolve
            )
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  }
}

const mutations = {
  [types.DISCREPANCIES](state, discrepancies) {
    state.discrepancies = discrepancies
  },
  [types.TOTAL_DISCREPANCIES](state, value) {
    state.totalDiscrepancies = value
  }
}

const state = {
  discrepancies: [],
  totalDiscrepancies: 0
}

export default {
  state,
  getters,
  actions,
  mutations
}
