import * as types from '@/store/mutation-types'
import api from '@/services/api/adminSiteConstraints'
import { buildSuccess, handleError } from '@/utils/utils.js'

const getters = {
  siteConstraints: (state) => state.siteConstraints,
  totalSiteConstraints: (state) => state.totalSiteConstraints
}

const actions = {
  getSiteConstraints({ commit }, payload) {
    return new Promise((resolve, reject) => {
      api
        .getSiteConstraints(payload)
        .then((response) => {
          if (response.status === 200) {
            // console.log(response.data)
            commit(types.SITE_CONSTRAINTS, response.data)
            commit(types.TOTAL_SITE_CONSTRAINTS, response.data.totalDocs)
            resolve()
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  },
  editSiteConstraint({ commit }, payload) {
    return new Promise((resolve, reject) => {
      const data = {
        name: payload.name
      }
      api
        .editSiteConstraint(payload._id, data)
        .then((response) => {
          if (response.status === 200) {
            buildSuccess(
              {
                msg: 'common.SAVED_SUCCESSFULLY'
              },
              commit,
              resolve
            )
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  },
  saveSiteConstraint({ commit }, payload) {
    return new Promise((resolve, reject) => {
      api
        .saveSiteConstraint(payload)
        .then((response) => {
          if (response.status === 201) {
            buildSuccess(
              {
                msg: 'common.SAVED_SUCCESSFULLY'
              },
              commit,
              resolve
            )
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  },
  deleteSiteConstraint({ commit }, payload) {
    return new Promise((resolve, reject) => {
      api
        .deleteSiteConstraint(payload)
        .then((response) => {
          if (response.status === 200) {
            buildSuccess(
              {
                msg: 'common.DELETED_SUCCESSFULLY'
              },
              commit,
              resolve
            )
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  }
}

const mutations = {
  [types.SITE_CONSTRAINTS](state, constraints) {
    state.siteConstraints = constraints
  },
  [types.TOTAL_SITE_CONSTRAINTS](state, value) {
    state.totalSiteConstraints = value
  }
}

const state = {
  siteConstraints: [],
  totalSiteConstraints: 0
}

export default {
  state,
  getters,
  actions,
  mutations
}
