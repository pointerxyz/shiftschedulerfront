import * as types from '@/store/mutation-types'
import api from '@/services/api/adminTaskEquipment'
import { buildSuccess, handleError } from '@/utils/utils.js'

const getters = {
  taskEquipments: (state) => state.taskEquipments,
  totalTaskEquipments: (state) => state.totalTaskEquipments
}

const actions = {
  getTaskEquipments({ commit }, payload) {
    return new Promise((resolve, reject) => {
      api
        .getTaskEquipments(payload)
        .then((response) => {
          if (response.status === 200) {
            commit(types.TASK_EQUIPMENTS, response.data.docs)
            commit(types.TOTAL_TASK_EQUIPMENTS, response.data.totalDocs)
            resolve()
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  },
  editTaskEquipment({ commit }, payload) {
    return new Promise((resolve, reject) => {
      const data = {
        name: payload.name
      }
      api
        .editTaskEquipment(payload._id, data)
        .then((response) => {
          if (response.status === 200) {
            buildSuccess(
              {
                msg: 'common.SAVED_SUCCESSFULLY'
              },
              commit,
              resolve
            )
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  },
  saveTaskEquipment({ commit }, payload) {
    return new Promise((resolve, reject) => {
      api
        .saveTaskEquipment(payload)
        .then((response) => {
          if (response.status === 201) {
            buildSuccess(
              {
                msg: 'common.SAVED_SUCCESSFULLY'
              },
              commit,
              resolve
            )
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  },
  deleteTaskEquipment({ commit }, payload) {
    return new Promise((resolve, reject) => {
      api
        .deleteTaskEquipment(payload)
        .then((response) => {
          if (response.status === 200) {
            buildSuccess(
              {
                msg: 'common.DELETED_SUCCESSFULLY'
              },
              commit,
              resolve
            )
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  }
}

const mutations = {
  [types.TASK_EQUIPMENTS](state, taskEquipments) {
    state.taskEquipments = taskEquipments
  },
  [types.TOTAL_TASK_EQUIPMENTS](state, value) {
    state.totalTaskEquipments = value
  }
}

const state = {
  taskEquipments: [],
  totalTaskEquipments: 0
}

export default {
  state,
  getters,
  actions,
  mutations
}
