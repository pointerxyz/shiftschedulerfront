import * as types from '@/store/mutation-types'
import api from '@/services/api/adminCumulhours.js'
import { buildSuccess, handleError } from '@/utils/utils.js'

const getters = {
  cumulHours: (state) => state.cumulHours,
  totalCumulHours: (state) => state.totalCumulHours
}

const actions = {
  getCumulHours({ commit }, payload) {
    return new Promise((resolve, reject) => {
      api
        .getCumulHours(payload)
        .then((response) => {
          if (response.status === 201) {
            // debugger
            // const cumulArray = []
            // response.data.forEach((element) => {
            //   cumulArray.push({
            //     cumulAgent: element.cumulAgent,
            //     cumulStart: element.cumulStart,
            //     cumulEnd: element.cumulEnd,
            //     cumulPlanned: element.cumulPlanned,
            //     cumulWorked: element.cumulWorked
            //   })
            // })
            // debugger
            // console.log(cumulArray)
            // console.log(response.data)
            commit(types.CUMUL_HOURS, response.data)
            commit(types.TOTAL_CUMUL_HOURS, response.data.totalDocs)
            resolve()
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  }
}

const mutations = {
  [types.CUMUL_HOURS](state, cumulHours) {
    state.cumulHours = cumulHours
  },
  [types.TOTAL_CUMUL_HOURS](state, value) {
    state.totalCumulHours = value
  }
}

const state = {
  cumulHours: [],
  totalCumulHours: 0
}

export default {
  state,
  getters,
  actions,
  mutations
}
