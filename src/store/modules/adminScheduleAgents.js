import * as types from '@/store/mutation-types'
import api from '@/services/api/adminScheduleAgents'
import * as moment from 'moment'
import { buildSuccess, handleError } from '@/utils/utils.js'

const getters = {
  scheduleagents: (state) => state.scheduleagents,
  totalScheduleagents: (state) => state.totalScheduleagents
}

const actions = {
  getScheduleagents({ commit }, payload) {
    return new Promise((resolve, reject) => {
      api
        .getScheduleagents(payload)
        .then((response) => {
          if (response.status === 200) {
            const scheduleArray = []
            // debugger
            // console.log(response.data)
            commit(types.SCHEDULE_AGENTS, response.data)
            commit(types.TOTAL_SCHEDULE_AGENTS, response.data.totalDocs)
            resolve()
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  },
  editScheduleagent({ commit }, payload) {
    return new Promise((resolve, reject) => {
      const data = {
        agent: payload.agent
      }
      api
        .editScheduleagent(payload._id, data)
        .then((response) => {
          if (response.status === 200) {
            buildSuccess(
              {
                msg: 'common.SAVED_SUCCESSFULLY'
              },
              commit,
              resolve
            )
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  },
  saveScheduleagent({ commit }, payload) {
    console.log(payload)
    return new Promise((resolve, reject) => {
      api
        .saveScheduleagent(payload)
        .then((response) => {
          if (response.status === 201) {
            buildSuccess(
              {
                msg: 'common.SAVED_SUCCESSFULLY'
              },
              commit,
              resolve
            )
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  },
  deleteScheduleagent({ commit }, payload) {
    debugger
    return new Promise((resolve, reject) => {
      api
        .deleteScheduleagent(payload)
        .then((response) => {
          if (response.status === 200) {
            buildSuccess(
              {
                msg: 'common.DELETED_SUCCESSFULLY'
              },
              commit,
              resolve
            )
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  }
}

const mutations = {
  [types.SCHEDULE_AGENTS](state, scheduleagents) {
    state.scheduleagents = scheduleagents
  },
  [types.TOTAL_SCHEDULE_AGENTS](state, value) {
    state.totalScheduleagents = value
  }
}

const state = {
  scheduleagents: [],
  totalScheduleagents: 0
}

export default {
  state,
  getters,
  actions,
  mutations
}
