import * as types from '@/store/mutation-types'
import api from '@/services/api/adminShiftConstraints'
import { buildSuccess, handleError } from '@/utils/utils.js'

const getters = {
  shiftConstraints: (state) => state.shiftConstraints,
  totalShiftConstraints: (state) => state.totalShiftConstraints
}

const actions = {
  getShiftConstraints({ commit }, payload) {
    return new Promise((resolve, reject) => {
      api
        .getShiftConstraints(payload)
        .then((response) => {
          if (response.status === 200) {
            // console.log(response.data)
            commit(types.SHIFT_CONSTRAINTS, response.data)
            commit(types.TOTAL_SHIFT_CONSTRAINTS, response.data.totalDocs)
            resolve()
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  },
  editShiftConstraint({ commit }, payload) {
    return new Promise((resolve, reject) => {
      const data = {
        name: payload.name
      }
      api
        .editShiftConstraint(payload._id, data)
        .then((response) => {
          if (response.status === 200) {
            buildSuccess(
              {
                msg: 'common.SAVED_SUCCESSFULLY'
              },
              commit,
              resolve
            )
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  },
  saveShiftConstraint({ commit }, payload) {
    return new Promise((resolve, reject) => {
      api
        .saveShiftConstraint(payload)
        .then((response) => {
          if (response.status === 201) {
            buildSuccess(
              {
                msg: 'common.SAVED_SUCCESSFULLY'
              },
              commit,
              resolve
            )
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  },
  deleteShiftConstraint({ commit }, payload) {
    return new Promise((resolve, reject) => {
      api
        .deleteShiftConstraint(payload)
        .then((response) => {
          if (response.status === 200) {
            buildSuccess(
              {
                msg: 'common.DELETED_SUCCESSFULLY'
              },
              commit,
              resolve
            )
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  }
}

const mutations = {
  [types.SHIFT_CONSTRAINTS](state, constraints) {
    state.shiftConstraints = constraints
  },
  [types.TOTAL_SHIFT_CONSTRAINTS](state, value) {
    state.totalShiftConstraints = value
  }
}

const state = {
  shiftConstraints: [],
  totalShiftConstraints: 0
}

export default {
  state,
  getters,
  actions,
  mutations
}
