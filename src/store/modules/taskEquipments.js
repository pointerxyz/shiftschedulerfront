import * as types from '@/store/mutation-types'
import api from '@/services/api/taskEquipments'
import { handleError } from '@/utils/utils.js'

const getters = {
  allTaskEquipments: (state) => state.allTaskEquipments,
  totalAllTaskEquipments: (state) => state.totalAllTaskEquipments
}

const actions = {
  getAllTaskEquipments({ commit }) {
    return new Promise((resolve, reject) => {
      api
        .getAllTaskEquipments()
        .then((response) => {
          if (response.status === 200) {
            // const array = response.data
            // const tasks = []
            // array.forEach(element => {
            //   tasks.push(element.taskName)
            // })
            // console.log(response.data)
            commit(types.FILL_ALL_TASK_EQUIPMENTS, response.data)
            commit(types.TOTAL_ALL_TASK_EQUIPMENTS, response.data.totalDocs)
            resolve()
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  }
}

const mutations = {
  [types.FILL_ALL_TASK_EQUIPMENTS](state, value) {
    state.allTaskEquipments = value
  },
  [types.TOTAL_ALL_TASK_EQUIPMENTS](state, value) {
    state.totalAllTaskEquipments = value
  }
}

const state = {
  allTaskEquipments: [],
  totalAllTaskEquipments: 0
}

export default {
  state,
  getters,
  actions,
  mutations
}
