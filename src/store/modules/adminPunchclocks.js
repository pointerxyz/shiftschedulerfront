import * as types from '@/store/mutation-types'
import api from '@/services/api/adminPunchclocks'
import { buildSuccess, handleError } from '@/utils/utils.js'

const getters = {
  punchclocks: (state) => state.punchclocks,
  totalPunchclocks: (state) => state.totalPunchclocks
}

const actions = {
  getPunchclocks({ commit }, payload) {
    return new Promise((resolve, reject) => {
      api
        .getPunchclocks(payload)
        .then((response) => {
          if (response.status === 201) {
            // const punchArray = []
            // response.data.forEach((element) => {
            //   punchArray.push({
            //     punchUsername: element.punchUsername,
            //     punchSite: element.punchSite,
            //     punchStart: element.punchStart,
            //     punchEnd: element.punchEnd,
            //     punchStartLocation: {
            //       Lng: element.punchStartLocation.coordinates[0],
            //       Lat: element.punchStartLocation.coordinates[1]
            //     },
            //     punchEndLocation: {
            //       Lng: element.punchEndLocation.coordinates[0],
            //       Lat: element.punchEndLocation.coordinates[1]
            //     }
            //   })
            // })
            console.log(response.data)
            commit(types.PUNCHCLOCKS, response.data)
            commit(types.TOTAL_PUNCHCLOCKS, response.data.totalDocs)
            resolve()
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  },
  validatePunches({ commit }, payload) {
    return new Promise((resolve, reject) => {
      api
        .validatePunches(payload)
        .then((response) => {
          if (response.status === 201) {
            // const punchArray = []
            // response.data.forEach((element) => {
            //   punchArray.push({
            //     punchUsername: element.punchUsername,
            //     punchSite: element.punchSite,
            //     punchStart: element.punchStart,
            //     punchEnd: element.punchEnd,
            //     punchStartLocation: {
            //       Lng: element.punchStartLocation.coordinates[0],
            //       Lat: element.punchStartLocation.coordinates[1]
            //     },
            //     punchEndLocation: {
            //       Lng: element.punchEndLocation.coordinates[0],
            //       Lat: element.punchEndLocation.coordinates[1]
            //     }
            //   })
            // })
            console.log(response.data)
            commit(types.PUNCHCLOCKS, response.data)
            commit(types.TOTAL_PUNCHCLOCKS, response.data.totalDocs)
            resolve()
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  }
}

const mutations = {
  [types.PUNCHCLOCKS](state, punchclocks) {
    state.punchclocks = punchclocks
  },
  [types.TOTAL_PUNCHCLOCKS](state, value) {
    state.total_punchclocks = value
  }
}

const state = {
  punchclocks: [],
  total_punchclocks: 0
}

export default {
  state,
  getters,
  actions,
  mutations
}
