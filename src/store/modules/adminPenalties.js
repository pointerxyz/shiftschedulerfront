import * as types from '@/store/mutation-types'
import api from '@/services/api/adminPenalties'
import * as moment from 'moment'
import { buildSuccess, handleError } from '@/utils/utils.js'

const getters = {
  penalties: (state) => state.penalties,
  totalPenalties: (state) => state.totalPenalties
}

const actions = {
  getPenalties({ commit }, payload) {
    return new Promise((resolve, reject) => {
      api
        .getPenalties(payload)
        .then((response) => {
          if (response.status === 200) {
            const penalties = []
            response.data.forEach((element) => {
              penalties.push({
                agent: element.agent,
                siteName: element.siteName,
                startSchedule: moment(element.start_schedule).format(
                  'DD/MM/YYYY'
                ),
                type: element.type,
                shift: element.penalty[0].shift,
                level: element.penalty[0].level,
                startDate: moment(element.penalty[0].start).format(
                  'DD/MM/YYYY'
                ),
                length: element.penalty[0].length,
                status: element.penalty[0].status,
                penalty: element.penalty[0].penalty
              })
            })
            console.log(penalties)
            commit(types.PENALTIES, penalties)
            commit(types.TOTAL_PENALTIES, response.data.totalDocs)
            resolve()
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  }
}

const mutations = {
  [types.PENALTIES](state, penalties) {
    state.penalties = penalties
  },
  [types.TOTAL_PENALTIES](state, value) {
    state.totalPenalties = value
  }
}

const state = {
  penalties: [],
  totalPenalties: 0
}

export default {
  state,
  getters,
  actions,
  mutations
}
