import * as types from '@/store/mutation-types'
import api from '@/services/api/adminScheduleAgents'
import * as moment from 'moment'
import { buildSuccess, handleError } from '@/utils/utils.js'

const getAbsoluteMonths = function (momentDate) {
  const months = Number(momentDate.format('MM'))
  const years = Number(momentDate.format('YYYY'))
  return months + years * 12
}

const getters = {
  agentschedules: (state) => state.agentschedules,
  totalAgentschedules: (state) => state.totalAgentschedules
}

const actions = {
  getAgentSchedules({ commit }, payload) {
    return new Promise((resolve, reject) => {
      api
        .getAgentSchedules(payload)
        .then((response) => {
          if (response.status === 200) {
            const today = moment(new Date())
            const todayMonths = getAbsoluteMonths(today)
            const scheduleArray = []
            response.data.forEach((element) => {
              const dateMonths = getAbsoluteMonths(
                moment(new Date(element.assign_start_shift))
              )
              const monthDifference = dateMonths - todayMonths
              if (monthDifference === 0) {
                scheduleArray.push({
                  assign__id: element._id,
                  assign_agent: element._agent,
                  assign_site: element.assign_site,
                  assign_start_shift: element.assign_start_shift,
                  assign_end_shift: element.assign_end_shift,
                  assign_shift: element.assign_shift,
                  assign_tasks: element.assign_tasks,
                  assign_observations: element.assign_observations
                })
              }
            })
            // debugger
            // console.log(response.data)
            commit(types.AGENT_SCHEDULES, scheduleArray)
            commit(types.TOTAL_AGENT_SCHEDULES, response.data.totalDocs)
            resolve()
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  },
  editAgentschedule({ commit }, payload) {
    return new Promise((resolve, reject) => {
      const data = {
        agent: payload.agent
      }
      api
        .editAgentschedule(payload._id, data)
        .then((response) => {
          if (response.status === 200) {
            buildSuccess(
              {
                msg: 'common.SAVED_SUCCESSFULLY'
              },
              commit,
              resolve
            )
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  },
  saveAgentschedule({ commit }, payload) {
    console.log(payload)
    return new Promise((resolve, reject) => {
      api
        .saveAgentschedule(payload)
        .then((response) => {
          if (response.status === 201) {
            buildSuccess(
              {
                msg: 'common.SAVED_SUCCESSFULLY'
              },
              commit,
              resolve
            )
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  },
  deleteAgentschedule({ commit }, payload) {
    debugger
    return new Promise((resolve, reject) => {
      api
        .deleteAgentschedule(payload)
        .then((response) => {
          if (response.status === 200) {
            buildSuccess(
              {
                msg: 'common.DELETED_SUCCESSFULLY'
              },
              commit,
              resolve
            )
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  }
}

const mutations = {
  [types.AGENT_SCHEDULES](state, agentschedules) {
    state.agentschedules = agentschedules
  },
  [types.TOTAL_AGENT_SCHEDULES](state, value) {
    state.totalAgentschedules = value
  }
}

const state = {
  agentschedules: [],
  totalAgentschedules: 0
}

export default {
  state,
  getters,
  actions,
  mutations
}
