import * as types from '@/store/mutation-types'
import api from '@/services/api/adminDiscrepancy'
import { buildSuccess, handleError } from '@/utils/utils.js'

const getters = {
  discrepancies: (state) => state.discrepancies,
  totalDiscrepancies: (state) => state.totalDiscrepancies
}

const actions = {
  getDiscrepancies({ commit }, payload) {
    return new Promise((resolve, reject) => {
      api
        .getDiscrepancies(payload)
        .then((response) => {
          if (response.status === 200) {
            console.log(response.data)
            commit(types.DISCREPANCIES, response.data.docs)
            commit(types.TOTAL_DISCREPANCIES, response.data.totalDocs)
            resolve()
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  }
}

const mutations = {
  [types.DISCREPANCIES](state, discrepancies) {
    state.discrepancies = discrepancies
  },
  [types.TOTAL_DISCREPANCIES](state, value) {
    state.totalDiscrepancies = value
  }
}

const state = {
  discrepancies: [],
  totalDiscrepancies: 0
}

export default {
  state,
  getters,
  actions,
  mutations
}
