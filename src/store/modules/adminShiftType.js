import * as types from '@/store/mutation-types'
import api from '@/services/api/adminShiftType'
import { buildSuccess, handleError } from '@/utils/utils.js'

const getters = {
  shiftTypes: (state) => state.shiftTypes,
  totalshiftTypes: (state) => state.totalshiftTypes
}

const actions = {
  getShiftTypes({ commit }, payload) {
    return new Promise((resolve, reject) => {
      api
        .getShiftTypes(payload)
        .then((response) => {
          if (response.status === 200) {
            commit(types.SHIFT_TYPES, response.data.docs)
            commit(types.TOTAL_SHIFT_TYPES, response.data.totalDocs)
            resolve()
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  },
  editShiftType({ commit }, payload) {
    return new Promise((resolve, reject) => {
      const data = {}
      api
        .editShiftType(payload._id, data)
        .then((response) => {
          if (response.status === 200) {
            buildSuccess(
              {
                msg: 'common.SAVED_SUCCESSFULLY'
              },
              commit,
              resolve
            )
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  },
  saveShiftType({ commit }, payload) {
    // console.log(payload)
    return new Promise((resolve, reject) => {
      api
        .saveShiftType(payload)
        .then((response) => {
          if (response.status === 201) {
            buildSuccess(
              {
                msg: 'common.SAVED_SUCCESSFULLY'
              },
              commit,
              resolve
            )
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  },
  deleteShiftType({ commit }, payload) {
    return new Promise((resolve, reject) => {
      api
        .deleteShiftType(payload)
        .then((response) => {
          if (response.status === 200) {
            buildSuccess(
              {
                msg: 'common.DELETED_SUCCESSFULLY'
              },
              commit,
              resolve
            )
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  }
}

const mutations = {
  [types.SHIFT_TYPES](state, shiftTypes) {
    state.shiftTypes = shiftTypes
  },
  [types.TOTAL_SHIFT_TYPES](state, value) {
    state.totalshiftTypes = value
  }
}

const state = {
  shiftTypes: [],
  totalshiftTypes: 0
}

export default {
  state,
  getters,
  actions,
  mutations
}
