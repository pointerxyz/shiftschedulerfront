import * as types from '@/store/mutation-types'
import api from '@/services/api/adminSiteSchedules'
import { buildSuccess, handleError } from '@/utils/utils.js'
const getters = {
  siteSchedules: (state) => state.siteSchedules,
  totalSiteSchedules: (state) => state.totalSiteSchedules
}

const actions = {
  getSiteSchedules({ commit }, payload) {
    return new Promise((resolve, reject) => {
      api
        .getSiteSchedules(payload)
        .then((response) => {
          if (response.status === 200) {
            // debugger
            // console.log(response.data)
            commit(types.SITE_SCHEDULES, response.data)
            commit(types.TOTAL_SITE_SCHEDULES, response.data.totalDocs)
            resolve()
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  },
  editSiteschedule({ commit }, payload) {
    console.log(payload)
    return new Promise((resolve, reject) => {
      const data = {
        id: payload.id,
        siteName: payload.siteName,
        siteDate: payload.siteDate,
        siteShift: payload.siteShift,
        siteAgents: payload.siteAgents
      }
      api
        .editSiteschedule(payload.id, data)
        .then((response) => {
          if (response.status === 200) {
            buildSuccess(
              {
                msg: 'common.SAVED_SUCCESSFULLY'
              },
              commit,
              resolve
            )
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  },
  saveSiteschedule({ commit }, payload) {
    console.log(payload)
    return new Promise((resolve, reject) => {
      api
        .saveSiteschedule(payload)
        .then((response) => {
          if (response.status === 201) {
            buildSuccess(
              {
                msg: 'common.SAVED_SUCCESSFULLY'
              },
              commit,
              resolve
            )
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  },
  deleteSiteschedule({ commit }, payload) {
    debugger
    return new Promise((resolve, reject) => {
      api
        .deleteSiteschedule(payload)
        .then((response) => {
          if (response.status === 200) {
            buildSuccess(
              {
                msg: 'common.DELETED_SUCCESSFULLY'
              },
              commit,
              resolve
            )
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  }
}

const mutations = {
  [types.SITE_SCHEDULES](state, siteSchedules) {
    state.siteSchedules = siteSchedules
  },
  [types.TOTAL_SITE_SCHEDULES](state, value) {
    state.totalSiteSchedules = value
  }
}

const state = {
  siteSchedules: [],
  totalSiteSchedules: 0
}

export default {
  state,
  getters,
  actions,
  mutations
}
