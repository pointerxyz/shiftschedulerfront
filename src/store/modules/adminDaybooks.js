import * as types from '@/store/mutation-types'
import api from '@/services/api/adminDaybooks'
import { buildSuccess, handleError } from '@/utils/utils.js'

const getters = {
  daybooks: (state) => state.daybooks,
  totalDaybooks: (state) => state.totalDaybooks
}

const actions = {
  getDaybooks({ commit }, payload) {
    return new Promise((resolve, reject) => {
      api
        .getDaybooks(payload)
        .then((response) => {
          if (response.status === 200) {
            console.log(response.data)
            // debugger
            commit(types.DAYBOOKS, response.data)
            commit(types.TOTAL_DAYBOOKS, response.data.totalDocs)
            resolve()
          }
        })
        .catch((error) => {
          handleError(error, commit, reject)
        })
    })
  }
}

const mutations = {
  [types.DAYBOOKS](state, daybooks) {
    state.daybooks = daybooks
  },
  [types.TOTAL_DAYBOOKS](state, value) {
    state.totalDaybooks = value
  }
}

const state = {
  daybooks: [],
  totalDaybooks: 0
}

export default {
  state,
  getters,
  actions,
  mutations
}
