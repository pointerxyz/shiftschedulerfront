module.exports = {
  productionSourceMap: false,
  transpileDependencies: [/node_modules[/\\\\]vuetify[/\\\\]/],
  pluginOptions: {
    i18n: {
      enableInSFC: true
    },
    electronBuilder: {
      builderOptions: {
        productName: 'Shift Scheduler'
      }
    }
  },
  devServer: {
    //disableHostCheck: true,
    host: '0.0.0.0',
    port: 3000,
    public: 'miradata.eu'
  },
  configureWebpack: {
    devtool: 'source-map'
  }
}
